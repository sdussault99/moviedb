document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDb();
    connexion.requeteTopRated();


});


class MovieDb {

    constructor() {

        this.APIkey = "ef57dca97791ca54b03b9082b4b3f129";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;

    }

    requeteTopRated() {
        var data = "{}";

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteTopRated.bind(this));

        xhr.open("GET", this.baseURL + "movie/top_rated?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send(data);
    }

    retourRequeteTopRated(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            // console.log(data);

            this.afficheTopRated(data);

        }
    }

    afficheTopRated(data) {

        for (let i=0; i < this.totalFilm; i++) {

            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerHTML = data[i].title;

            if(data[i].overview == "") {
                unArticle.querySelector(".description").innerHTML = "Pas de description";
            }
            else {
                unArticle.querySelector(".description").innerHTML = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);




            document.querySelector(".liste-films").appendChild(unArticle);
        }
    }


}
document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDb();
    connexion.requeteTopRated();


});


class MovieDb {

    constructor() {

        this.APIkey = "ef57dca97791ca54b03b9082b4b3f129";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;

    }

    requeteTopRated() {
        var data = "{}";

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteTopRated.bind(this));

        xhr.open("GET", this.baseURL + "movie/top_rated?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send(data);
    }

    retourRequeteTopRated(e) {

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            // console.log(data);

            this.afficheTopRated(data);

        }
    }

    afficheTopRated(data) {

        for (let i=0; i < this.totalFilm; i++) {

            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerHTML = data[i].title;

            if(data[i].overview == "") {
                unArticle.querySelector(".description").innerHTML = "Pas de description";
            }
            else {
                unArticle.querySelector(".description").innerHTML = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);




            document.querySelector(".liste-films").appendChild(unArticle);
        }
    }


}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xuXG5cbiAgICB2YXIgY29ubmV4aW9uID0gbmV3IE1vdmllRGIoKTtcbiAgICBjb25uZXhpb24ucmVxdWV0ZVRvcFJhdGVkKCk7XG5cblxufSk7XG5cblxuY2xhc3MgTW92aWVEYiB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcblxuICAgICAgICB0aGlzLkFQSWtleSA9IFwiZWY1N2RjYTk3NzkxY2E1NGIwM2I5MDgyYjRiM2YxMjlcIjtcblxuICAgICAgICB0aGlzLmxhbmcgPSBcImZyLUNBXCI7XG5cbiAgICAgICAgdGhpcy5iYXNlVVJMID0gXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL1wiO1xuXG4gICAgICAgIHRoaXMuaW1nUGF0aCA9IFwiaHR0cDovL2ltYWdlLnRtZGIub3JnL3QvcC9cIjtcblxuICAgICAgICB0aGlzLmxhcmdldXJBZmZpY2hlID0gW1wiOTJcIiwgXCIxNTRcIiwgXCIxODVcIiwgXCIzNDJcIiwgXCI1MDBcIiwgXCI3ODBcIl07XG5cbiAgICAgICAgdGhpcy5sYXJnZXVyVGV0ZUFmZmljaGUgPSBbXCI0NVwiLCBcIjE4NVwiXTtcblxuICAgICAgICB0aGlzLnRvdGFsRmlsbSA9IDg7XG5cbiAgICAgICAgdGhpcy50b3RhbEFjdGV1ciA9IDY7XG5cbiAgICB9XG5cbiAgICByZXF1ZXRlVG9wUmF0ZWQoKSB7XG4gICAgICAgIHZhciBkYXRhID0gXCJ7fVwiO1xuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJSZXF1ZXRlVG9wUmF0ZWQuYmluZCh0aGlzKSk7XG5cbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS90b3BfcmF0ZWQ/cGFnZT0xJmxhbmd1YWdlPVwiICsgdGhpcy5sYW5nICsgXCImYXBpX2tleT1cIiArIHRoaXMuQVBJa2V5KTtcblxuICAgICAgICB4aHIuc2VuZChkYXRhKTtcbiAgICB9XG5cbiAgICByZXRvdXJSZXF1ZXRlVG9wUmF0ZWQoZSkge1xuXG4gICAgICAgIGxldCB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG5cbiAgICAgICAgbGV0IGRhdGE7XG5cbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xuXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KS5yZXN1bHRzO1xuXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlVG9wUmF0ZWQoZGF0YSk7XG5cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFmZmljaGVUb3BSYXRlZChkYXRhKSB7XG5cbiAgICAgICAgZm9yIChsZXQgaT0wOyBpIDwgdGhpcy50b3RhbEZpbG07IGkrKykge1xuXG4gICAgICAgICAgICBsZXQgdW5BcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5hcnRpY2xlLmZpbG1cIikuY2xvbmVOb2RlKHRydWUpO1xuXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImgyXCIpLmlubmVySFRNTCA9IGRhdGFbaV0udGl0bGU7XG5cbiAgICAgICAgICAgIGlmKGRhdGFbaV0ub3ZlcnZpZXcgPT0gXCJcIikge1xuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLmRlc2NyaXB0aW9uXCIpLmlubmVySFRNTCA9IFwiUGFzIGRlIGRlc2NyaXB0aW9uXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kZXNjcmlwdGlvblwiKS5pbm5lckhUTUwgPSBkYXRhW2ldLm92ZXJ2aWV3O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgdW5lSW1hZ2UgPSB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImltZ1wiKTtcblxuICAgICAgICAgICAgdW5lSW1hZ2Uuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwid1wiICsgdGhpcy5sYXJnZXVyQWZmaWNoZVszXSArIGRhdGFbaV0ucG9zdGVyX3BhdGgpO1xuXG5cblxuXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWZpbG1zXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XG4gICAgICAgIH1cbiAgICB9XG5cblxufSJdLCJmaWxlIjoic2NyaXB0LmpzIn0=
